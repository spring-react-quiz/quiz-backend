create table if not exists orders (
    id BIGINT AUTO_INCREMENT,
    goodsId BIGINT not null ,
    amount BIGINT not null,
    primary key (id)
);