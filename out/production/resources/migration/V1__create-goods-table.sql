create table if not exists goods (
    id BIGINT AUTO_INCREMENT,
    name varchar (24) not null,
    unit varchar(10) not null,
    image varchar(128) not null,
    primary key (id)
);