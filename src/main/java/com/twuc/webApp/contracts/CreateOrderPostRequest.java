package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateOrderPostRequest {
    @NotNull
    private Long goodsId;

    @NotNull
    private Long amount;

    public Long getGoodsId() {
        return goodsId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
