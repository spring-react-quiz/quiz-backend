package com.twuc.webApp.contracts;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateGoodsPostRequest {
    @NotNull
    @Size(min = 1, max = 24)
    private String name;

    @NotNull
    private Long price;

    @NotNull
    @Size(min = 1, max = 10)
    private String unit;

    @NotNull
    @Size(min = 1, max = 128)
    private String image;

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
