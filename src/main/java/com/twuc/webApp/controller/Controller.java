package com.twuc.webApp.controller;

import com.twuc.webApp.contracts.CreateGoodsPostRequest;
import com.twuc.webApp.contracts.CreateOrderPostRequest;
import com.twuc.webApp.entities.Goods;
import com.twuc.webApp.entities.Orders;
import com.twuc.webApp.repositories.GoodsRepository;
import com.twuc.webApp.repositories.OrderRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class Controller {
    private final GoodsRepository goodsRepository;

    private final OrderRepository orderRepository;

    private final EntityManager entityManager;

    public Controller(GoodsRepository goodsRepository, OrderRepository orderRepository, EntityManager entityManager) {
        this.goodsRepository = goodsRepository;
        this.orderRepository = orderRepository;
        this.entityManager = entityManager;
    }

    @PostMapping("/goods")
    public ResponseEntity uploadGoods(@RequestBody @Valid CreateGoodsPostRequest goodsPostRequest) {
        if (goodsPostRequest == null) return ResponseEntity.badRequest().build();
        Boolean isExist = goodsRepository.findByName(goodsPostRequest.getName()).isPresent();
        if (isExist) return ResponseEntity.badRequest().build();
        goodsRepository.saveAndFlush(
                new Goods(
                        goodsPostRequest.getName(),
                        goodsPostRequest.getPrice(),
                        goodsPostRequest.getUnit(),
                        goodsPostRequest.getImage()
                )
        );
        entityManager.clear();
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/goods")
    public ResponseEntity<List<Goods>> getAllGoods() {
        List<Goods> allGoods = goodsRepository.findAll();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(allGoods);
    }

    @PostMapping("/orders")
    public ResponseEntity createOrder(@RequestBody @Valid CreateOrderPostRequest orderPostRequest) {
        if (null == orderPostRequest) return ResponseEntity.badRequest().build();
        boolean isExist = orderRepository.findByGoodsId(orderPostRequest.getGoodsId()).isPresent();
        if (isExist) return this.updateOrder(orderPostRequest);

        orderRepository.saveAndFlush(new Orders(
                orderPostRequest.getGoodsId(),
                orderPostRequest.getAmount()
        ));
        entityManager.clear();

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/orders")
    public ResponseEntity updateOrder(@RequestBody @Valid CreateOrderPostRequest orderPostRequest) {
        if (null == orderPostRequest) return ResponseEntity.badRequest().build();
        Orders orders = orderRepository.findByGoodsId(orderPostRequest.getGoodsId()).orElseThrow(RuntimeException::new);
        orders.setAmount(orders.getAmount() + orderPostRequest.getAmount());

        orderRepository.saveAndFlush(orders);
        entityManager.clear();

        return ResponseEntity.ok().build();
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Orders>> getAllOrders() {
        List<Orders> allOrders = orderRepository.findAll();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(allOrders);
    }

    @DeleteMapping("/order/{orderId}")
    public ResponseEntity deleteOrder(@PathVariable Long orderId) {
        orderRepository.deleteById(orderId);
        return ResponseEntity.ok().build();
    }
}
