package com.twuc.webApp.entities;

import javax.persistence.*;

@Entity(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "goodsId", nullable = false)
    private Long goodsId;

    @Column(name = "amount", nullable = false)
    private Long amount;

    public Orders() {
    }

    public Orders(Long goodsId, Long amount) {
        this.goodsId = goodsId;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
