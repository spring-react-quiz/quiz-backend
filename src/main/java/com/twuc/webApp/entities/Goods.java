package com.twuc.webApp.entities;

import javax.persistence.*;

@Entity(name = "goods")
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false, length = 24)
    private String name;

    @Column(name = "price", nullable = false)
    private Long price;

    @Column(name = "unit", nullable = false, length = 10)
    private String unit;

    @Column(name = "image", nullable = false, length = 128)
    private String image;

//    @ManyToOne
//    @JoinColumn(name = "goodsId")
//    private Order order;


    public Goods() {
    }

    public Goods(String name, Long price, String unit, String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }

//    public Order getOrder() {
//        return order;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public void setOrder(Order order) {
//        this.order = order;
//    }
}
