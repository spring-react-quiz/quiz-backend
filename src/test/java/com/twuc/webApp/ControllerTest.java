package com.twuc.webApp;

import com.twuc.webApp.entities.Goods;
import com.twuc.webApp.entities.Orders;
import com.twuc.webApp.repositories.GoodsRepository;
import com.twuc.webApp.repositories.OrderRepository;
import com.twuc.webApp.service.TestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ControllerTest extends TestBase {
    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void should_upload_goods_and_return_201() throws Exception {
        Goods goods = new Goods("可乐", 3L,"瓶", "https://imgservice.suning.cn/uimg1/b2c/image/QjCpHipFtt9OEp78h7uv8A==.jpg_800w_800h_4e");

        getMockMvc().perform(
                post("http://localhost/api/goods")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(goods))
        )
                .andExpect(status().is(201));
    }

    @Test
    void should_get_all_goods_and_return_200() throws Exception {
        Goods goods = new Goods("可乐", 3L,"瓶", "https://imgservice.suning.cn/uimg1/b2c/image/QjCpHipFtt9OEp78h7uv8A==.jpg_800w_800h_4e");
        Goods anotherGoods = new Goods("CocoCola", 3L,"瓶", "https://imgservice.suning.cn/uimg1/b2c/image/QjCpHipFtt9OEp78h7uv8A==.jpg_800w_800h_4e");

        flushAndClear(em -> {
            goodsRepository.save(goods);
            goodsRepository.save(anotherGoods);
        });

        getMockMvc().perform(get("http://localhost/api/goods"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.size()").value(2));
    }

    @Test
    void should_add_goods_to_order_and_return_201() throws Exception {
        Goods goods = new Goods("Cola", 3L, "Bottle", "src");
        flushAndClear(em -> {
            goodsRepository.save(goods);
        });
        Goods getGoods = goodsRepository.findById(1L).orElseThrow(RuntimeException::new);
        Orders order = new Orders(getGoods.getId(), 1L);
        getMockMvc().perform(
                post("http://localhost/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(order))
        )
                .andExpect(status().is(201));
    }

    @Test
    void should_get_all_orders_and_return_200() throws Exception {
        Goods goods = new Goods("Cola", 3L, "Bottle", "src");
        flushAndClear(em -> {
            goodsRepository.save(goods);
        });
        Goods getGoods = goodsRepository.findById(1L).orElseThrow(RuntimeException::new);
        Orders order = new Orders(getGoods.getId(), 1L);
        flushAndClear(em -> {
            orderRepository.save(order);
        });

        getMockMvc().perform(get("http://localhost/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.size()").value(1));
    }

    @Test
    void should_delete_specified_order_and_return_200() throws Exception {
        Goods goods = new Goods("Cola", 3L, "Bottle", "src");
        flushAndClear(em -> {
            goodsRepository.save(goods);
        });
        Goods getGoods = goodsRepository.findById(1L).orElseThrow(RuntimeException::new);
        Orders order = new Orders(getGoods.getId(), 1L);
        flushAndClear(em -> {
            orderRepository.save(order);
        });

        getMockMvc().perform(delete("http://localhost/api/order/1"))
                .andExpect(status().is(200));
    }
}